<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VariantRepository")
 * @ORM\Table(name="variants")
 */
class Variant
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $correctly;

    /**
     * @ORM\ManyToOne(targetEntity="Question", inversedBy="variants")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    protected $question;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->correctly = false;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Variant
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set correctly
     *
     * @param boolean $correctly
     * @return Variant
     */
    public function setCorrectly($correctly)
    {
        $this->correctly = $correctly;

        return $this;
    }

    /**
     * Get correctly
     *
     * @return boolean 
     */
    public function getCorrectly()
    {
        return $this->correctly;
    }

    /**
     * Set question
     *
     * @param \AppBundle\Entity\Question $question
     * @return Variant
     */
    public function setQuestion(\AppBundle\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \AppBundle\Entity\Question 
     */
    public function getQuestion()
    {
        return $this->question;
    }
}
