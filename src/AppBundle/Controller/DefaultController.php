<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/tests", name="tests")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $tests = $em->getRepository('AppBundle:Test')->findAll();
        return $this->render('AppBundle:Default:index.html.twig', array(
            'tests' => $tests
        ));

    }

    /**
     * @Route("/tests/{id}", name="test_one")
     * @Template()
     */
    public function testAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $test = $em->getRepository('AppBundle:Test')->findOneById($id);
        $questions = $test->getQuestions();
        return $this->render('AppBundle:Default:test.html.twig', array(
            'test' => $test,
            'questions' => $questions
        ));

    }
}
