<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Variant;
use AppBundle\Form\VariantType;

/**
 * Variant controller.
 *
 * @Route("admin/variant")
 */
class VariantController extends Controller
{

    /**
     * Lists all Variant entities.
     *
     * @Route("/", name="variant")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Variant')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Variant entity.
     *
     * @Route("/", name="variant_create")
     * @Method("POST")
     * @Template("AppBundle:Variant:new.html.twig")
     */
    public function createAction(Request $request, $id)
    {   $em = $this->getDoctrine()->getManager();
        $question = $em->getRepository('AppBundle:Question')->findOneById($id);

        $entity = new Variant();
        $entity = setQuestion($question);
        $form = $this->createCreateForm($entity,$id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('question_edit', array('id' =>$id)));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Variant entity.
     *
     * @param Variant $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Variant $entity)
    {
        $form = $this->createForm(new VariantType(), $entity, array(
            'action' => $this->generateUrl('variant_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Variant entity.
     *
     * @Route("/new", name="variant_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Variant();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Variant entity.
     *
     * @Route("/{id}/edit", name="variant_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Variant')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Variant entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Variant entity.
    *
    * @param Variant $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Variant $entity)
    {
        $form = $this->createForm(new VariantType(), $entity, array(
            'action' => $this->generateUrl('variant_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Variant entity.
     *
     * @Route("/{id}", name="variant_update")
     * @Method("PUT")
     * @Template("AppBundle:Variant:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Variant')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Variant entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('variant_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Variant entity.
     *
     * @Route("/{id}", name="variant_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Variant')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Variant entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('variant'));
    }

    /**
     * Creates a form to delete a Variant entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('variant_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
